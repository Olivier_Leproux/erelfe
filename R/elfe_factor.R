# ==========#
# Fonctionne#----
# ==========#


#' elfe_factor
#'
#' @param bdd An Elfe dataframe
#' @param v Name of variables to be recoded. By default, all variables in the dataframe.
#' @param verbose Default is "FALSE". "TRUE" displays the name and format of the variable to be recoded, as well as its summary. TRUE is particularly useful for identifying variables that may cause the function to bug.
#'
#' @return Returns a data frame in which actual categorical variables are represented as categorical (factor). Modalities remain in numerical form (1,2,3, etc.). Errors may occur. Check information. To function, the mapping table built by the elfe_init function must be located in the working directory.
#' @export
#'
#'
#'



elfe_factor <- function(bdd, v = names(bdd), verbose = FALSE) {
  table_elfe <- chgt()

  if (verbose == FALSE) { # condition verbose == TRUE
    pb <-  utils::txtProgressBar(min = 0, max = length(v), initial = 0, style = 3)
  }


  # Initialize progress bar


  for (z in v) { # z = nom des variable du vecteur v, vecteur de noms de variable à recoder

    # ===============================================#
    # verification que la variable est bien à recoder#
    # ===============================================#
    if (verbose == FALSE) { # condition verbose == TRUE
      utils::setTxtProgressBar(pb,  utils::getTxtProgressBar(pb) + 1)
    }

    verif <- z[(z %in% table_elfe[["label_code_moda"]]$code_var)] # Verification que la variable est bien dans la liste des modalites à recoder
    if (length(verif) == 0) next # si pas dans la liste, passer à la variable suivante


    extract_code_moda <- table_elfe[["label_code_moda"]] %>% dplyr::filter(code_var == z) # Extraction de la variable z à recoder
    extract_code_moda <- unique(extract_code_moda)

    if (verbose == TRUE) { # condition verbose == TRUE

      if (is.na(extract_code_moda$code_moda) == TRUE) { # debut de la condition, la variable ne possede pas de format ==  TRUE
        info_recid <- paste0("\n The \"", extract_code_moda$code_var, "\" variable has no format. It is probably a numeric variable that will not be recoded.\n")
        cat(info_recid)
      } # Fin de la condition la variable possede un format ==  TRUE

      if (is.na(extract_code_moda$code_moda) == FALSE) { # debut de la condition, la variable possede un format
        info_recid <- paste0("\n Attempt to recode the \"", extract_code_moda$code_var, "\" in \"", extract_code_moda$code_moda, "\" format.")
        cat(info_recid)
      } # fin de la condition, la variable possede un format
    } # Fin de la condition verbose = TRUE



    if (is.na(extract_code_moda[1, "code_moda"])) { # si pas de label,        #==> Ne rien faire
    } else {
      bdd[[z]] <- labelled::to_character(bdd[[z]])
      bdd[[z]] <- labelled::to_factor(bdd[[z]])



      lab <- stringr::str_remove_all(extract_code_moda$label, "\"")
      lab <- unique(lab)
      labelled::var_label(bdd[[z]]) <- lab

      if (verbose == TRUE) { # condition verbose == TRUE

        if (is.na(extract_code_moda$code_moda) == FALSE) { # debut de la condition, la variable possede un format
          info_recid <- paste0("\n Result of variable recoding \"", extract_code_moda$code_var, "\"   whose format is \"", extract_code_moda$code_moda, "\".\n")
          cat(info_recid)

          res <- summary(bdd[[z]])
          print(res)
        } # fin de la condition, la variable possede un format
      } # Fin de la condition verbose = TRUE
    }
  } # fin boucle z

  if (verbose == FALSE) { # condition verbose == TRUE
    close(pb)
  }
  warning("Errors may occur. Please check information.")
  return(bdd)
} # fin de fonction elfe_factor
