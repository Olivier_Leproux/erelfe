
# erelfe

<!-- badges: start -->
<!-- badges: end -->

This package is designed to facilitate the labeling and recoding of data
from the Elfe cohort. It also provides a variable dictionary adapted to
the variable names of this cohort. Several functions in this package are
based on a mapping table built with the elfe_init function.

## Function

This package includes six functions: elfe_dico_skim, elfe_init,
elfe_factor, elfe_infomoda, elfe_init, elfe_label and elfe_txt.

### 1/ elfe_dico_skim

Returns a dictionary summarizing the data constructed with the “skim”
function of the “skimr” package, with adaptations to the specificities
of the Elfe cohort: age of the survey wave, type of interlocutor
(mother, doctor, father, etc.), label, main part of the variable name to
identify variables filled in at different ages, etc.

### 2/ elfe_init

Returns The mapping table required to execute the “elfe_txt”,
“elfe_label”, “elfe_info” and “elfe_fact” functions.

### 3/ elfe_label

Returns a labelled data frame.

### 4/ elfe_factor

Returns a data frame in which normally categorical variables become
categorical (factor). Modalities remain in numerical form (1,2,3, etc.).
Errors may occur. Check information.

### 5 /elfe_txt

Returns a data framework in which nominally categorical variables become
categorical. Numerical modalities are transformed into textual
modalities. Errors may occur. Please check information.

### 6/ elfe_infomoda

The console displays the modalities in numerical form and their
correspondence with the modalities in text form, as well as a summary of
the variables. No action is taken on the data.
